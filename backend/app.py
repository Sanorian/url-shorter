from fastapi import FastAPI
from fastapi.responses import JSONResponse
from fastapi.middleware.cors import CORSMiddleware
from random import randint
from typing import Union
from pydantic import BaseModel
from mysql.connector import connect
from mysql.connector.errors import Error
import json

chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789'

app = FastAPI()

app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

class getNewLinkModel(BaseModel):
    url: str

@app.post("/", response_class = JSONResponse)
def get_new_link(model: getNewLinkModel):
    try:
        with connect(
            host="database",
            port=3306,
            user="user",
            password="jznsv3la",
            database="URL_Database"
        ) as connection:
            with connection.cursor() as cursor:
                cursor.execute("SELECT token FROM urls WHERE url=%s;", (model.url,))
                data = cursor.fetchone()
                if data is None:
                    while True:
                        short_link = get_short_link()
                        if not is_short_link_used(short_link, connection):
                            cursor.execute("INSERT INTO urls (token, url) VALUES (%s, %s);", (short_link, model.url))
                            connection.commit()
                            break
                    return JSONResponse(content={"res": "good", "short_link": short_link})
                else:
                    return JSONResponse(content= {"res": "good", "short_link": data[0]})
    except Error as e:
        print(e)
        return JSONResponse(content={"res": "bad", "error": e})

def is_short_link_used(short_link, connection):
    with connection.cursor() as cursor:
        cursor.execute("SELECT url FROM urls WHERE token=%s;", (short_link,))
        data = cursor.fetchone()
        if data is None:
            return False
        else:
            return True

def get_short_link():
    short_link = ""
    for i in range(0, 6):
        short_link += chars[randint(0, len(chars) - 1)]
    return short_link