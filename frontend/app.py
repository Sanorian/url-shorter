from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
from fastapi.responses import HTMLResponse, RedirectResponse, JSONResponse
import aiofiles
from mysql.connector import connect
from mysql.connector.errors import Error
import json

app = FastAPI()

app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

index = None
error = None
does_not_exist = None

@app.on_event("startup")
async def startup_event():
    global index, error, does_not_exist
    async with aiofiles.open('pages/index.html', mode='r') as f:
        index = await f.read()
    async with aiofiles.open('pages/error.html', mode='r') as f:
        error = await f.read()
    async with aiofiles.open('pages/does_not_exist.html', mode='r') as f:
        does_not_exist = await f.read()


@app.get('/', response_class=HTMLResponse)
async def index():
    global index
    return index

@app.get('/error', response_class=HTMLResponse)
async def error():
    global error
    return error

@app.get('/doesnotexist', response_class=HTMLResponse)
async def does_not_exist():
    global does_not_exist
    return does_not_exist

@app.get('/{token}', response_class=RedirectResponse)
def get_link(token: str):
    try:
        with connect(
            host="database",
            port=3306,
            user="user",
            password="jznsv3la",
            database="URL_Database"
        ) as connection:
            with connection.cursor() as cursor:
                query = "SELECT url FROM urls WHERE token=%s;"
                cursor.execute(query, (token,))
                data = cursor.fetchone()
                print(data)
                if data is not None:
                    return RedirectResponse(url=data[0])
                else:
                    return RedirectResponse(url='/doesnotexist')
    except Error as e:
        print(e)
        return RedirectResponse(url='/error')
